"""

1. Add requirements.txt for all necessary libs to easy install it with pip
2. Some kind of progress bar when request is processed ( check if given link is valid, before green or red popup )
3. Html changes to make web app look better
4. Add transfer speed info of task
5. Batch script or even app ( in python ) which make deployment process easier - it will run on Windows and
   install all libs with pip, change settings in system to start server in background every time when system is
   booting and ask for location where files will be download before first run
6. Maybe you find some bugs then you can try to fix them :D

"""
